package fr.afpa.authentification;

import java.io.IOException;
import java.io.PipedWriter;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RecupPwd
 */
public class RecupPwd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println(this.getServletContext());
		
		// R�cup�ration du nombre de param�tres � afficher
		String nombre = request.getParameter("nbParamAafficher");
		// Conversion String ==> Integer
		int nombreDigit = 0;
		// V�rifier si la chaine de caract�re n'est pas nulle est ne contient que des digits
		if (nombre != null && nombre.matches("^[0-9]+$")) {
			nombreDigit = Integer.parseInt(nombre);
		}

		// Compteur pour arr�ter la boucle
		int compteur = 0;
		
		System.out.println("Liste de mots de passe :");
		// R�cup�ration des noms de param�tres d'initialisation
		// (<param-name></param-name>)
		Enumeration<String> listeParamsInit = getServletConfig().getInitParameterNames();

		PrintWriter out = response.getWriter();
		// Parcourir la liste de
		while (listeParamsInit.hasMoreElements() && compteur<nombreDigit) {
			// R�cup�ration des valeurs de param�tres d'initialisation
			// (<param-value></param-value>)
			String str = (String) listeParamsInit.nextElement();
			// Pour afficher les noms de param�tres et leurs valeurs ds la console
			System.out.println("Compte : " + str + "  Mot de passe : " + getServletConfig().getInitParameter(str));

			// Pour ajouter les noms de param�tres et leurs valeurs ds la r�ponse HTTP
			out.println("Compte : " + str + " |||  Mot de passe : " + getServletConfig().getInitParameter(str)
					+ " <======> ");
			
			// Incr�mentation du compteur 
			compteur++;

		}

	}

}
