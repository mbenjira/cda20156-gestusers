package fr.afpa.authentification;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Personne;

/**
 * Servlet implementation class EnvoyerAttributsAJSP
 */
public class EnvoyerAttributsAJSP extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		// Ajouter des attributs dans la requ�te
		request.setAttribute("ABC","BENJIRA");
		request.setAttribute("prenom","Mohammed");
		request.setAttribute("age","10 ans");
		
		
		Personne per = new Personne("TOTO", "TATA" , 20);
		request.setAttribute("maPersonne",per);
		
		// Redirection de l'utilisateur vers la page exempleRecupeAttributs.jsp avec l'objet request 
		// qui contient 3 attributs � savoir nom, prenom et age
		RequestDispatcher dispatcher = request.getRequestDispatcher("exempleRecupeAttributs.jsp");
		dispatcher.forward(request, response);
	}

}
