package fr.afpa.authentification;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Authentification
 */
public class Authentification extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Récupération des informations saisies par l'utilisateur
		String loginForm = request.getParameter("loginUtilisateur");
		String passForm = request.getParameter("passUtilisateur");
		
		//Récupération de la valeur de la balise <param-value>Valeur</param-value>
		String pass = getInitParameter(loginForm);
		
		
		request.setAttribute("login", loginForm);
		
		// Utilisation de l'objet de redirection
		// Pour plus d'info : http://teaching-java.nextnet.top/javaee_web/request_dispatcher.html
		RequestDispatcher disp = null;
		String nextVue ="maPage.jsp";
		
//		if(pass != null && passForm.contentEquals(pass)) {
//			nextVue = "accueil.html";
//		}
//		else {
//			System.out.println("ERRRRRRRRROOOOOOOOOORRRRRRRRR !!!!!!");
//			nextVue = "index.jsp";
//		}
		
		disp = request.getRequestDispatcher(nextVue);
		disp.forward(request, response);
		
	}

}
