<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.lang.String" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
      <form action="Authentification" method="post">
		<div class="container">
			<label for="uname"><b>Username</b></label> 
			<input type="text" placeholder="Enter Username" name="loginUtilisateur" required> 
			<label for="psw"><b>Password</b></label> 
			<input type="password" placeholder="Enter Password" name="passUtilisateur" required>
			<button type="submit">Login</button>
			<label> <input type="checkbox" checked="checked" name="remember"> Remember me
			</label>
		</div>
	</form>
       <br>
      <a href="RecupPwd?nbParamAafficher=2">Afficher les mots de passe</a>
      <br>
      <a href="coursJavaJSP.pdf">Télécharger cours</a>
    </body>
</html>
